#!/usr/bin/env sh

currentVolume=$(pulsemixer --get-volume | cut -d ' ' -f 1)
isMute=$(pulsemixer --get-mute)

if [ "$isMute" -eq 0 ]; then
    volumeIcon=""
else
    volumeIcon=""
fi

echo "$volumeIcon $currentVolume%"
