(define-package "zetteldeft" "20210919.1306" "Turn deft into a zettelkasten system"
  '((emacs "25.1")
    (deft "0.8")
    (ace-window "0.7.0"))
  :commit "f4f227a9cdb69cf06fd3715e40ddf17a069063f1" :authors
  '(("EFLS <Elias Storms>"))
  :maintainer
  '("EFLS <Elias Storms>")
  :keywords
  '("deft" "zettelkasten" "zetteldeft" "wp" "files")
  :url "https://efls.github.io/zetteldeft/")
;; Local Variables:
;; no-byte-compile: t
;; End:
