#!/usr/bin/env sh

backupName="android-full-backup_$(date '+%Y-%m-%d').ab"

adb backup -apk -noshared -all -f "$backupName"

sha1sum "$backupName" > "$backupName".sha1