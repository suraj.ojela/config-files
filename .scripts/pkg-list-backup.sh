#!/usr/bin/env zsh

# Creates and saves a list of pacman packages. Only creates a new file if the previous one is outdated.

backupDir="$BACKUP_DIR"/Arch/pkg/
current=$(comm -23 <(pacman -Qeq | sort) <(pacman -Qgq base-devel | sort))
if [ "$(ls -A $backupDir)" ]; then
	previousFile="$backupDir$(ls $backupDir | tail -n 1)"
	diff=`diff $previousFile =(echo $current)`
	if [ "$diff" != '' ]; then
		echo $current > ${backupDir}arch_apps`date '+%Y-%m-%d_%H-%M-%S'`.txt
	fi
else
	echo $current > ${backupDir}arch_apps`date '+%Y-%m-%d_%H-%M-%S'`.txt
fi