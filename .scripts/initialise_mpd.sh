#!/usr/bin/env sh

while [ "MPD error: Connection refused" = "$(mpc)" ]; do
	sleep 1
done

pulsemixer --set-volume 35
mpc volume 100
mpc clear && mpc ls | grep -E "mp3|flac" | mpc add
mpc random on
mpc repeat on
mpc play
