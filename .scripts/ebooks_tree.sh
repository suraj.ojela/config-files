#!/usr/bin/env sh

# Creates and saves a list of ebooks using tree. Only creates a new file if the previous one is outdated.

backupDir="$BACKUP_DIR"/Ebooks
current=$(tree "$HOME"/Nextcloud/)
lastBackup=$(find "$backupDir"/* -type f | tail -n 1)
if [ "$lastBackup" != '' ]; then
	previous=$(cat "$lastBackup")
	if [ "$current" != "$previous" ]; then
		echo "$current" > "${backupDir}"/ebooks_list"$(date '+%Y-%m-%d_%H-%M-%S')".txt
	fi
	exit 0
fi

echo "$current" > "${backupDir}/ebooks_list$(date '+%Y-%m-%d_%H-%M-%S').txt"