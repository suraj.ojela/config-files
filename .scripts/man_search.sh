#!/usr/bin/env sh

# Search man pages with fzf

manPage="$(apropos --section=1 --long "$1" | fzf --exact | awk '{print $1}' | tr -d '()')"

if [ -n "$manPage" ]; then
    man "$manPage"
fi
