#!/usr/bin/env sh

directory="."
hashFile="hashes.txt"

while getopts ":d:h:" options; do
    case "${options}" in
	d) directory="${OPTARG}";;
	h) hashFile="${OPTARG}";;
	*) echo "Unknown option" && exit 2
    esac
done

[ ! -d "$directory" ] && echo "Invalid directory" && exit 1

if [ -f "$hashFile" ]; then
    read -p "The hash file exists. Overwrite? (y/n)" yn
    case $yn in
	[!Yy]* ) exit;;
    esac
fi

hashdeep -c sha1 -r -e -o f "$directory"/Android\ modding "$directory/"Art  "$directory/"Backups  "$directory/"Documents  "$directory/"Music  "$directory/"New\ Folder  "$directory/"OS  "$directory/"Pictures  "$directory/"Windows\ Installers | tee "$hashFile"