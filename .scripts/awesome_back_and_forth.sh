#!/usr/bin/env sh

# Changes the current tag then runs a program. When the program finishes the tag is changed to the active tag prior to running the program.

if [ $# -lt 2 ]; then
    echo usage: "$0" tagIndex program [ programOption1 ] [ programOption2 ] ...
    echo tagIndex starts at 1 and must be valid
    exit
fi

currentTag=$(echo 'local awful = require("awful"); return awful.screen.focused().selected_tag.index' \
                 | awesome-client \
                 | awk -F ' ' '{print $2}')

destinationTag=$1

relativeTag=$((destinationTag - currentTag))

echo 'local awful = require("awful"); return awful.tag.viewidx('"$relativeTag"')' | awesome-client

# Remove index from argument list
shift 1
$("$@")

originalTag=$((-1 * relativeTag))

echo 'local awful = require("awful"); return awful.tag.viewidx('"$originalTag"')' | awesome-client