#!/usr/bin/env sh

# Find and execute script with rofi.

script=$(find "$SCRIPTS_DIR" -type f -exec basename {} \; | rev | cut -d'.' -f2- | rev | rofi -dmenu -matching fuzzy)

if [ -n "$script" ]; then
   scriptWithPath=$(find "$SCRIPTS_DIR" -name "$script.*" -type f)
   echo $scriptWithPath
   $TERMINAL -e "$SCRIPTS_DIR/spawn_shell_hack.sh" "$scriptWithPath"
fi