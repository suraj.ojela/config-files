HISTSIZE=10000
SAVEHIST=10000
unsetopt beep
bindkey -v

zstyle :compinstall filename '~/.config/zsh/.zshrc'

autoload -Uz compinit
compinit

setopt HIST_IGNORE_ALL_DUPS # delete duplicates
setopt HIST_REDUCE_BLANKS # remove superfluous blanks from history items
setopt COMPLETE_ALIASES
setopt AUTO_CD
setopt APPEND_HISTORY
setopt HIST_SAVE_NO_DUPS
setopt HIST_FIND_NO_DUPS
setopt EXTENDED_HISTORY # save timestamp and duration of commands
setopt INC_APPEND_HISTORY # immediately add items to history
setopt PROMPT_SUBST # enable command subtitution in prompt

source /usr/share/fzf/key-bindings.zsh
source /usr/share/fzf/completion.zsh

PLUGINSDIR="$HOME"/.config/zsh/plugins

source $PLUGINSDIR/fast-syntax-highlighting/fast-syntax-highlighting.plugin.zsh
source $PLUGINSDIR/zsh-colored-man-pages/colored-man-pages.plugin.zsh
source $PLUGINSDIR/zsh-autopair/zsh-autopair.plugin.zsh
source $PLUGINSDIR/zsh-autosuggestions/zsh-autosuggestions.plugin.zsh
source $PLUGINSDIR/zsh-interactive-cd/zsh-interactive-cd.plugin.zsh
source $PLUGINSDIR/zsh-system-clipboard/zsh-system-clipboard.plugin.zsh
source $PLUGINSDIR/zsh-vim-mode/zsh-vim-mode.plugin.zsh
source $PLUGINSDIR/zsh-command-time/command-time.plugin.zsh

export PROMPT='%F{yellow}%3~ %(?.%F{magenta}$.%F{red}$) '

alias rm='trash-put'
alias mv='mv -i'
alias cp='cp -i'
alias grep='grep --color=auto'
alias tri='trickle -s -d'
alias yayt='tri 300 yay'
alias y='yay-fzf'
alias yr='yay-uninstall-fzf'
alias yqg='yay -Q | rg'
alias ls='exa -a'
alias l='exa -la'
alias t='tremc'
alias v='nvim'
alias vsudo='sudo EDITOR="nvim" visudo'
alias upgrade='$SCRIPTS_DIR/arch_update.sh'
alias g='git'
alias less='nvim -R'
alias grip='grip -b'
alias c='git --git-dir=$HOME/.dotfiles --work-tree=$HOME'
alias ca='git-fzf-add-dotfiles'
alias cca='git-fzf-add-patch-dotfiles'
alias cco='git-fzf-checkout "--git-dir=$HOME/.dotfiles --work-tree=$HOME"'
alias e='emacs --no-window-system'
alias z='$SCRIPTS_DIR/zettelkasten-tag-search.sh'
alias sc='zsh -c $SCRIPTS_DIR/$(ls $SCRIPTS_DIR | fzf)'
alias yda='youtube-dl --continue --ignore-errors --extract-audio --audio-format mp3 -o "%(title)s.%(ext)s"'
alias rgain='mp3gain -r -c *.mp3'
alias ss='setsid -f'

# kill processes - list only the ones you can kill.
fkill() {
    local pid
    if [ "$UID" != "0" ]; then
        pid=$(ps -f -u $UID | sed 1d | fzf -m | awk '{print $2}')
    else
        pid=$(ps -ef | sed 1d | fzf -m | awk '{print $2}')
    fi

    if [ "x$pid" != "x" ]
    then
        echo $pid | xargs kill
    fi
}

git-fzf-diff-dotfiles() {
    fzfPreviewCommand="cd $HOME; git --git-dir=$HOME/.dotfiles --work-tree=$HOME diff --color=always -- {-1}"

    eval "git --git-dir=$HOME/.dotfiles --work-tree=$HOME diff --name-status --ignore-submodules=dirty" | fzf --preview="${fzfPreviewCommand}" --bind=alt-n:preview-page-down,alt-p:preview-page-up | awk '//{print $2}'
}

git-fzf-add-dotfiles() {
    file=$(git-fzf-diff-dotfiles)
    if [ -n "$file" ]; then
        git --git-dir="$HOME"/.dotfiles --work-tree="$HOME" add "$HOME/$file"
    fi
}

git-fzf-add-patch-dotfiles() {
    file=$(git-fzf-diff-dotfiles)
    if [ -n "$file" ]; then
        git --git-dir="$HOME"/.dotfiles --work-tree="$HOME" add --patch "$HOME/$file"
    fi
}

git-fzf-checkout() {
    branch=$(eval "git $@ branch | sed 's/^..//' | cut -d' ' -f1 | sort | fzf")

    if [ -n "$branch" ]; then
        eval "git $@ checkout $branch"
    fi
}

yay-fzf() {yay -Slq | fzf -m --preview 'yay -Si {1}'| xargs -ro trickle -s -d 300 yay -S}
yay-uninstall-fzf() {yay -Qeq | fzf -m --preview 'yay -Qi {1}' | xargs -ro yay -Rs}
extract() {$SCRIPTS_DIR/extract.sh "$1"}
wttr() {curl wttr.in/"$@"}

# expand aliases with a space
expand-alias() {
	zle _expand_alias
	zle self-insert
}
zle -N expand-alias
bindkey -M main ' ' expand-alias

# fzf history search
bindkey -M viins '^r' fzf-history-widget
bindkey -M vicmd '^r' fzf-history-widget

zstyle ':completion:*' rehash true # auto find new executables
zstyle ':completion:*' menu select # navigate suggestions with arrow keys

# use jj to exit insert mode
bindkey -M viins 'jj' vi-cmd-mode
export KEYTIMEOUT=100

# include hidden files in auto complete
_comp_options+=(globdots)

# edit command in editor
autoload -U edit-command-line
zle -N edit-command-line
bindkey -M vicmd v edit-command-line

# search previous commands using arrow keys
autoload -U up-line-or-beginning-search
autoload -U down-line-or-beginning-search
zle -N up-line-or-beginning-search
zle -N down-line-or-beginning-search
bindkey "^[[A" up-line-or-beginning-search # Up
bindkey "^[[B" down-line-or-beginning-search # Down

# TTY font
setfont iso02-12x22.psfu.gz 2>/dev/null