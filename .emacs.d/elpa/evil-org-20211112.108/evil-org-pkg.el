(define-package "evil-org" "20211112.108" "evil keybindings for org-mode"
  '((emacs "24.4")
    (evil "1.0"))
  :commit "c3ec94bc2fb79127826ea85509247f082bc394aa" :maintainer
  '("Somelauw")
  :keywords
  '("evil" "vim-emulation" "org-mode" "key-bindings" "presets")
  :url "https://github.com/Somelauw/evil-org-mode.git")
;; Local Variables:
;; no-byte-compile: t
;; End:
