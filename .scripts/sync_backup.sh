#!/usr/bin/env sh

# Backup syncthing directory if it has changed

lastBackup=$(find "$SYNC_DIR"/* -type f | tail -n 1)
lastHash=$(sha1sum "$lastBackup" | awk '{print $1}')
currentHash=$(tar -czf - /home/cosmicanimecat/Sync/ 2>/dev/null | sha1sum | awk '{print $1}')
if [ "$lastHash" != "$currentHash" ]; then
	tar -czvf "$BACKUP_DIR/Sync/sync_$(date  '+%Y-%m-%d_%H-%M-%S')".tgz "$SYNC_DIR/"
fi