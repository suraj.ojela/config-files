#!/usr/bin/env sh

backupDir="/mnt/backup/OS Backup/"
backupName="arch_$(date '+%Y-%m-%d').img"

dd if=/dev/sda of="$backupDir$backupName" bs=64K conv=noerror,sync status=progress

sha1sum "$backupDir$backupName" > "$backupDir$backupName".sha1