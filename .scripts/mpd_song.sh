#!/usr/bin/env sh

# Get file name of current MPD song without its extension

PLAYLIST=$(mpc | wc -l)

if [ "$PLAYLIST" -ne 1 ]; then
    PAUSED=$(mpc | awk 'NR==2' | grep paused)

    if [ -z "$PAUSED" ]; then
        mpc -f %file% | head -n 1 | rev | cut -f 2- -d '.' | rev
    else
        echo MPD paused
    fi
fi
