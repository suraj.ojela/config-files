#!/usr/bin/env sh

# Creates and saves a list of python packages installed with pip. Only creates a new file if the previous one is outdated.

backupDir="$BACKUP_DIR"/Arch/pip/
current=$(python -m pip list)

if [ -n "$(ls -A "$backupDir")" ]; then
	  previousFile="$(find "$backupDir" -type f | tail -n 1)"
	  previous=$(cat "$previousFile")
	  if [ "$previous" != "$current" ]; then
	  	    echo "$current" > "${backupDir}pip_packages$(date '+%Y-%m-%d_%H-%M-%S')".txt
	  fi
else
	  echo "$current" > "${backupDir}pip_packages$(date '+%Y-%m-%d_%H-%M-%S')".txt
fi