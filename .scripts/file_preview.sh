#!/usr/bin/env sh

[ $# -ne 2 ] \
    && echo Incorrect number of arguments \
    && echo Usage: "$0" file columns \
    && exit 1

if [ -d "$1" ]; then
    tree -L 3 -F "$1" | cut -c 1-"$2"
    exit
fi

getMimeType() { file --mime-type "$1" | awk -F' ' '{print $NF}'; }

case "$1" in
    # audio
    *.wav | *.mp3 | *.flac | *.m4a | *.wma | *.ape | *.ac3 | *.og[agx] | *.spx | *.opus)
        mediainfo "$1"
        ;;
    # video
    *.avi | *.mp4 | *.wmv | *.dat | *.3gp | *.ogv | *.mkv | *.mpg | *.mpeg | *.vob | *.fl[icv] | *.m2v | *.mov | *.webm | *.ts | *.mts | *.m4v | *.r[am] | *.qt | *.divx | *.as[fx])
          "$SCRIPTS_DIR/video_info.sh" "$1"
          ;;
    # Markdown
    *.md) glow -s dark "$1";;
    # PDF
    *.pdf) pdftotext -l 3 -nopgbrk "$1" -;;
    # Images
    *.bmp | *.jpg | *.jpeg | *.png | *.gif | *.xpm)
        mediainfo "$1"
        ;;
    # Torrent
    *.torrent) transmission-show "$1";;
    # Office files
    *.odt | *.odp | *.sxw) odt2txt "$1";;
    *.doc) catdoc "$1";;
    *.docx)
        unzip-mem -p "$1" word/document.xml | sed -e 's/<\/w:p>/\n/g; s/<[^>]\{1,\}>//g; s/[^[:print:]\n]\{1,\}//g'
        ;;
    # Default
    *)
       mimeType=$(getMimeType "$1")

       isText=$(echo "$mimeType" | grep "text")

       if [ -n "$isText" ]; then
           highlight --input="$1" --stdout --out-format=xterm256 --force
       else
           echo No preview
       fi
esac