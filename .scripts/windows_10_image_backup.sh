#!/usr/bin/env sh

backupDir="/mnt/backup/OS Backup/"
backupName="windows_10_$(date '+%Y-%m-%d').img"

dd if=/dev/sdb bs=64K conv=noerror,sync status=progress of="$backupDir$backupName"

sha1sum "$backupDir$backupName" > "$backupDir$backupName".sha1