#!/usr/bin/env sh

rsync --update --archive --delete "$HOME/Art/" "$STORAGE_DIR/Art/"
rsync --update --archive --delete "$HOME/Nextcloud/Ebooks/" "$STORAGE_DIR/Documents/Ebooks/"
rsync --update --archive --delete "$HOME/Notes/" "$STORAGE_DIR/Documents/Notes/"