#!/usr/bin/env sh

# search zettelkasten tags with fzf and display all files with the tag

# tags should be on a single line and be the first line to match the regex "^#\w"

FILE_PATTERN="*.org"

tags="$(grep --no-filename --max-count=1 --regexp="^#\w" $NOTES_DIR/$FILE_PATTERN)"

uniqueTags=$(echo "$tags" | sed 's/ /\n/g' | sed '/^[[:space:]]*$/d' | sort | uniq)

tag=$(echo "$uniqueTags" | fzf)

# exit if no tag is selected
[ -z "$tag" ] && exit

# edge cases ignored for speed
# TODO don't show full path
grep --files-with-matches --max-count=1 --extended-regexp --regexp="${tag}(\s|\')" $NOTES_DIR/$FILE_PATTERN