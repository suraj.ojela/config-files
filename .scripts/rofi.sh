#!/usr/bin/env sh

rofi -modi 'run,drun,window' -show run -matching fuzzy
