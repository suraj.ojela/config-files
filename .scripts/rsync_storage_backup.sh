#!/usr/bin/env sh

backupDir="/run/media/cosmicanimecat/Backup/Storage/"

rsync --exclude="\$RECYCLE.BIN" \
      --exclude=".Trash-1000" \
      --exclude="System Volume Information" \
      --delete-excluded \
      --progress --verbose --archive --update --delete "$STORAGE_DIR/" "$backupDir"