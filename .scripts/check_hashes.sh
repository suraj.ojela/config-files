#!/usr/bin/env sh

directory="/run/media/cosmicanimecat/Backup/Storage"
hashFile="hashes.txt"

while getopts ":d:h:" options; do
    case "${options}" in
	d) directory=${OPTARG};;
	h) hashFile=${OPTARG};;
	*) echo "Unknown option" && exit 2
    esac
done

[ ! -d "$directory" ] && echo "Invalid directory" && exit 1
[ ! -f "$hashFile" ] && echo "Hash file does not exist" && exit 3

hashdeep -c sha1 -k "$hashFile" -r -s -o f -a -v "$directory"/Android\ modding "$directory"/Art "$directory"/Backups "$directory"/Documents "$directory"/Music "$directory"/New\ Folder "$directory"/OS "$directory"/Pictures "$directory"/Windows\ Installers