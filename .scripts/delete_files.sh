#!/usr/bin/env sh

# Given a directory and a number, trashes files in the directory
# alphabetically until only the given number of files remain

[ $# -ne 2 ] &&
    echo Incorrect number of arguments &&
    echo Usage: "$0" directory numberOfFilesToKeep &&
    exit 1

[ ! -d "$1" ] &&
    echo Directory does not exist &&
    exit 2

numberOfFilesInDir=$(find "$1" -maxdepth 1 -type f | wc -l)

if [ "$numberOfFilesInDir" -eq 0 ] || [ "$numberOfFilesInDir" -le "$2" ]; then
    echo No files to delete
    exit
fi

find "$1" -maxdepth 1 -type f | sort | head -n $((numberOfFilesInDir - $2)) | xargs trash-put