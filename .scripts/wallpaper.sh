#!/usr/bin/env sh

# Set wallpaper

feh --bg-scale --no-fehbg ~/.config/wallpaper.*
