#!/usr/bin/env sh

# Use this script to create an interactive terminal after running a command

"$@"
"$SHELL"