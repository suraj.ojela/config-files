#!/usr/bin/env sh

# Generate an image of a random arithmetic problem

while getopts ":amdsth" options; do
    case "${options}" in
	a)
	    # defaults
	    range="10000-99999"
	    numberOfNumbers=5

	    while getopts ":r:n:" subOptions; do
		case "${subOptions}" in
		    r) range=${OPTARG};;
		    n) numberOfNumbers=${OPTARG};;
		    *) >&2 echo error && exit 1
		esac
	    done

	    sum=0
	    while [ "$numberOfNumbers" -gt 0 ]; do
		numberOfNumbers=$((numberOfNumbers-1))

		temp=$(shuf -i "$range" -n 1)

		if [ $sum = 0 ]; then
		    firstNumber=$temp
		else
		    if [ ${#numbers} = 0 ]; then
			numbers=$temp
		    else
			numbers=$(printf "%s\n%s" "$numbers" "$temp")
		    fi
		fi

		sum=$((sum + temp))
	    done

	    printf "Ans: %s\n\n%s\n%s" "$sum" "$firstNumber" "$numbers" \
		| convert label:@- "addition_$(date +"%Y-%m-%dT%H%M%S").jpg"
	    ;;

	m)
	    # defaults
	    multiplierRange="100-999"
	    multiplicandRange="100000-999999"

	    while getopts ":r:d:" subOptions; do
		case "${subOptions}" in
		    r) multiplierRange=${OPTARG};;
		    d) multiplicandRange=${OPTARG};;
		    *) >&2 echo error && exit 1
		esac
	    done

	    multiplicand=$(shuf -i "$multiplicandRange" -n 1)
	    multiplier=$(shuf -i "$multiplierRange" -n 1)
	    answer=$((multiplicand * multiplier))

	    printf "Ans: %s\n\n%s * %s" "$answer" "$multiplicand" "$multiplier" \
		| convert label:@- "multiplication_$(date +"%Y-%m-%dT%H%M%S").jpg"
	    ;;

	d)
	    # defaults
	    dividendRange="100000-999999"
	    divisorRange="100-999"

	    while getopts ":e:r:" subOptions; do
		case "${subOptions}" in
		    e) dividendRange=${OPTARG};;
		    r) divisorRange=${OPTARG};;
		    *) >&2 echo error && exit 1
		esac
	    done

	    dividend=$(shuf -i "$dividendRange" -n 1)
	    divisor=$(shuf -i "$divisorRange" -n 1)
	    quotient=$((dividend / divisor))
	    remainder=$((dividend - (divisor * quotient)))


	    printf "Ans: %s r %s\n\n%s / %s" "$quotient" "$remainder" "$dividend" "$divisor" \
		| convert label:@- "division_$(date +"%Y-%m-%dT%H%M%S").jpg"
	    ;;

	s)
	    # defaults
	    range="100-999"

	    while getopts ":r:" subOptions; do
		case "${subOptions}" in
		    r) range=${OPTARG};;
		    *) >&2 echo error && exit 1
		esac
	    done

	    number=$(shuf -i "$range" -n 1)
	    answer=$((number * number))

	    printf "Ans: %s\n\n%s^2" "$answer" "$number" \
		| convert label:@- "square_$(date +"%Y-%m-%dT%H%M%S").jpg"
	    ;;

	t)
	    # defaults
	    range="10000000-99999999"

	    while getopts ":r:" subOptions; do
		case "${subOptions}" in
		    r) range=${OPTARG};;
		    *) >&2 echo error && exit 1
		esac
	    done

	    number=$(shuf -i "$range" -n 1)
	    wholePartAnswer=$(awk -v x="$number" 'BEGIN{print sqrt(x)}' | awk -F. '{print $1}')
	    remainder=$((number - wholePartAnswer * wholePartAnswer))

	    printf "Ans: %s r %s\n\nsqrt %s" "$wholePartAnswer" "$remainder" "$number" \
		| convert label:@- "root_$(date +"%Y-%m-%dT%H%M%S").jpg"
	    ;;

	h)
	    echo "Usage: $0 arithmeticOption [OPTIONS]
-a addition
Suboptions:
-r range
-n number of numbers

-m multiplication
Suboptions:
-r multiplier range
-d multiplicand range

-d division
Suboptions:
-e dividend range
-r divisor range

-s squaring
Suboptions:
-r range

-t square root
Suboptions:
-r range"
	    ;;

	*)
	    >&2 echo error
	    exit 1
    esac
done