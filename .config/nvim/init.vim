" Vim plug
" Specify a directory for plugins
call plug#begin('~/.local/share/nvim/plugged')

	Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' } " Completion framework
	Plug 'Shougo/neco-syntax' " Syntax source for deoplete
	Plug 'Shougo/deoplete-clangx' " Clang syntax source for deoplete
	Plug 'deoplete-plugins/deoplete-zsh' " ZSH syntax source for deoplete
	Plug 'kovetskiy/sxhkd-vim' " sxhkd syntax highlighting and indenting
	Plug 'neomake/neomake' " Linting and make

	Plug 'SirVer/ultisnips' " Snippet engine
	Plug 'honza/vim-snippets' " Snippets

	Plug 'airblade/vim-gitgutter' " Show git diff
	Plug 'jreybert/vimagit' " Git

	Plug 'christoomey/vim-tmux-navigator' " Navigate between tmux and vim seamlessly
	Plug 'easymotion/vim-easymotion' " Improved motions
	Plug 'ervandew/supertab' " Insert completion with tab
	Plug 'jiangmiao/auto-pairs' " Insert/delete/etc brackets/quotes in pairs
	Plug 'junegunn/fzf.vim' " Fuzzy file search
	Plug 'kshenoy/vim-signature' " Marks
	Plug 'lambdalisue/suda.vim' " Sudo
	Plug 'lervag/vimtex' " Various features for Latex documents
	Plug 'luochen1990/rainbow' " Add colour to all brackets
	Plug 'tpope/vim-capslock' " Software caps lock
	Plug 'tpope/vim-commentary' " Commenting
	Plug 'tpope/vim-projectionist' " Switch between alternate files
	Plug 'tpope/vim-repeat' " Add support for repeating plugin commands
	Plug 'tpope/vim-surround' " Surround with brackets etc.
	Plug 'tpope/vim-unimpaired' " Various keybindings
	Plug 'vim-airline/vim-airline' " Statusline
	Plug 'vim-scripts/SearchComplete' " Tab completion inside searches
  	Plug 'chrisbra/Colorizer' " Highlight colours
  	Plug 'dhruvasagar/vim-table-mode' " ASCII tables
	Plug 'sjl/gundo.vim' " Graphical undo
	Plug 'https://github.com/tyru/open-browser.vim' " Open urls

" Initialize plugin system
call plug#end()

function GroffMomToPdf()
	let l:pdfname = expand('%:p:r') . ".pdf"
	execute "!" . "pdfmom" . " '" . bufname("%") . "' > '" . l:pdfname . "'"
endfunction

function OpenTerminalTab()
	tabedit
	terminal
endfunction

function CreateZettelkastenNote()
	let newNote=system('~/.scripts/zettelkasten-create-note.sh')
	execute "tabedit ".newNote
endfunction

filetype plugin on " Load plugins for filetype
filetype indent on " Load indent file for filetype
syntax enable " Syntax highlighting

let mapleader = " "

colorscheme ron

set mouse=a " Enable the mouse in all modes
set number relativenumber " Relative line numbers except the current, which is absolute
set splitright splitbelow " Open new splits to the right or below
set guicursor=n-v-c:block-Cursor-blinkon1,i-ci:ver100-iCursor-blinkon1 " Blinking cursor
set lazyredraw " Don't update screen during macro etc.
set clipboard+=unnamedplus " Use clipboard for all operations
set hlsearch " Highlight all search matches
set ignorecase smartcase " Ignore case unless search term has capital letters
set guioptions=M " Don't source unnecessary file for gui

nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l
inoremap jj <Esc>
nnoremap <leader>fc :FZF .<CR>
nnoremap <leader>ff :FZF ~<CR>
nnoremap <leader>fr :History<CR>
nnoremap <leader>fz :FZF ~/Sync/Notes<CR>
nnoremap <leader>bu :GundoToggle<CR>
nnoremap <leader>bs :setlocal spell! spelllang=en_gb<CR>
nnoremap <leader>bt :call OpenTerminalTab()<CR>
nnoremap <leader>bb :tab sball<CR>
nnoremap <leader>zn :call CreateZettelkastenNote()<CR>
tnoremap <c-space> <c-\><c-n>

autocmd BufWritePost *.mom silent! :call GroffMomToPdf()

autocmd BufWritePre * %s/\s\+$//e " Remove trailing white space
autocmd BufWritepre * %s/\n\+\%$//e " Remove trailing newlines

" Reload externally changed file
augroup checktime
    autocmd!
    autocmd BufEnter        * silent! checktime
    autocmd CursorHold      * silent! checktime
    autocmd CursorHoldI     * silent! checktime
    autocmd CursorMoved     * silent! checktime
    autocmd CursorMovedI    * silent! checktime
augroup END

let g:fzf_layout = { 'window': 'enew' } " Make fzf use entire window

let g:rainbow_active = 1

let g:colorizer_auto_color = 1

call neomake#configure#automake('nrwi', 100)

let g:deoplete#enable_at_startup = 1

let g:suda#prefix = 'sudo:'

let g:airline_symbols_ascii = 1

" Vim-repeat
silent! call repeat#set("\<Plug>[<Space>", v:count)
silent! call repeat#set("\<Plug>]<Space>", v:count)

" UltiSnips
let g:UltiSnipsExpandTrigger="<c-space>"
let g:UltiSnipsJumpForwardTrigger="<leader>n"
let g:UltiSnipsJumpBackwardTrigger="<leader>p"
let g:UltiSnipsListSnippets = "<c-[>"

" Open urls
let g:netrw_nogx = 1 " disable netrw's gx mapping
nmap gx <Plug>(openbrowser-smart-search)
vmap gx <Plug>(openbrowser-smart-search)