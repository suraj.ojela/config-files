#!/usr/bin/env sh

python -m pip list --outdated --format=freeze --user | grep -v '^\-e' | cut -d = -f 1  | xargs -n1 python -m pip install --upgrade --user