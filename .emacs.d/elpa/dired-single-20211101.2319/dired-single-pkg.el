;;; Generated package description from dired-single.el  -*- no-byte-compile: t -*-
(define-package "dired-single" "20211101.2319" "Reuse the current dired buffer to visit a directory" 'nil :stars '(#("7" 0 1 (font-lock-face paradox-star-face))) :commit "b254f9b7bfc96a5eab5760a56811f2872d2c590a" :keywords '("dired" "reuse" "buffer") :url "https://github.com/crocket/dired-single")
