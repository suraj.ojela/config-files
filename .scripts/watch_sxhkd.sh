#!/usr/bin/env sh

# Rerun sxhkd when its config changes

echo ~/.config/sxhkd/sxhkdrc | entr -psn 'pkill sxhkd; sxhkd &'