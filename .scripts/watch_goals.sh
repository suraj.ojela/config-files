#!/usr/bin/env sh

# Convert goals.org to pdf when it changes

echo "$SYNC_DIR"/Goals/goals.org | \
    entr -psn 'pandoc -f org -t pdf "$SYNC_DIR"/Goals/goals.org > "$SYNC_DIR"/Goals/goals.pdf &'