#!/usr/bin/env sh

[ $# -eq 0 ] &&
    echo Error: incorrect number of arguments &&
    echo Usage: "$0" [file1].. &&
    exit 1

# Check all files have same extention
if [ $# -gt 1 ]; then
    extension=$(echo "$1" | rev | cut -d '.' -f1)
    for file in "$@"; do
        currentExtension=$(echo "$file" | rev | cut -d '.' -f1)

        [ "$extension" != "$currentExtension" ] &&
            echo Error: different file types &&
            exit 2

    done
fi

case "$1" in
    # Inkscape
    *.svg) inkscape "$1" &;;
    # Mom
    *.mom) pdfmom "$1" | zathura - &;;
    # Pdf
    *.pdf) zathura "$@" &;;
    # mobi
    *.mobi) zathura "$@" &;;
    # PostScript
    *.ps | *.eps | *.ps.gz) zathura "$@" &;;
    # Djvu
    *.djvu) zathura "$@" &;;
    # Epub
    *.epub) zathura "$@" &;;
    # Audio
    *.wav | *.mp3 | *.flac | *.m4a | *.wma | *.ape | *.ac3 | *.og[agx] | *.spx | *.opus)
	"$SCRIPTS_DIR/awesome_back_and_forth.sh" 10 mpv --player-operation-mode=pseudo-gui "$@" 2>/dev/null &;;
    # Video
    *.avi | *.mp4 | *.wmv | *.dat | *.3gp | *.ogv | *.mkv | *.mpg | *.mpeg | *.vob | *.fl[icv] | *.m2v | *.mov | *.webm | *.ts | *.mts | *.m4v | *.r[am] | *.qt | *.divx | *.as[fx])
        "$SCRIPTS_DIR/awesome_back_and_forth.sh" 10 mpv "$@" 2>/dev/null &;;
    # Web
    *.html | *.htm) firefox "$@" &;;
    # Object
    *.o) nm "$1" | less;;
    # Images
    *.bmp | *.jpg | *.jpeg | *.png | *.gif | *.xpm | *.webp) qimgv "$@" &;;
    # Hashes
    *.md5) md5sum -c "$1";;
    *.sha1) sha1sum -c "$1";;
    *.sha256) sha256sum -c "$1";;
    *.sha512) sha512sum -c "$1";;
    # Torrent
    *.torrent) transmission-add-file "$@";;
    # Office files
    *.ods | *.odt | *.doc | *.docx | *.xls | *.xlsx | *.odp | *.pptx) libreoffice "$@" &;;
    # Qt projects
    *.pro) qtcreator "$@" &;;
    # Archive
    *.zip | *.jar | *.war | *.ear | *.oxt | *.apkg) zip -sf "$1" | less;;
    *.7z | *.rar | *.iso) 7z l "$1" | less;;
    *.tar | *.tar.gz | *.tgz | *.tar.bz2 | *.tbz2 | *.tar.xz | *.txz) tar --list --verbose --file "$1" | less;;
    # Default
    *) "$EDITOR" "$@"
esac