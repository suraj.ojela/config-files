#!/usr/bin/env sh

# Get current CPU usage percentage and load average

pad_space() {
    spacesToAdd=$(($2 - ${#1}))
    padded=$1

    while [ $spacesToAdd -gt 0 ]; do
        padded=" $padded"
        spacesToAdd=$((spacesToAdd - 1))
    done

    echo "$padded"
}

cpu_usage=$(top -b -n1 -p 1 | grep -F "Cpu(s)" | tail -1 | awk -F'id,' -v prefix="$prefix" '{ split($1, vs, ","); v=vs[length(vs)]; sub("%", "", v); printf "%s%.1f%%\n", prefix, 100 - v }')

load_avg_1_min=$(awk '//{print $1}' /proc/loadavg)
load_avg_5_min=$(awk '//{print $2}' /proc/loadavg)
load_avg_15_min=$(awk '//{print $3}' /proc/loadavg)

load_avg_1_min=$(pad_space "$load_avg_1_min" 6)
load_avg_5_min=$(pad_space "$load_avg_5_min" 6)
load_avg_15_min=$(pad_space "$load_avg_15_min" 6)
cpu_usage=$(pad_space "$cpu_usage" 7)

echo "$cpu_usage$load_avg_1_min$load_avg_5_min$load_avg_15_min"
