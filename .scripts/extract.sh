#!/usr/bin/env sh

# Extract various file formats to their own directory

[ $# -ne 1 ] &&
    echo Incorrect number of arguments &&
    echo Usage: "$0" file &&
    exit 1

case "$1" in
    *.tar.gz | *.tar.bz2)
        dirName="$(echo "$1" | rev | cut -d'.' -f 3- | rev)"
        mkdir "$dirName" &&
            tar -xzf "$1" -C "$dirName" &&
            echo Extracted files to "$dirName"
        ;;
    *.tar.xz)
        dirName="$(echo "$1" | rev | cut -d'.' -f 3- | rev)"
        mkdir "$dirName" &&
            tar -xf "$1" -C "$dirName" &&
            echo Extracted files to "$dirName"
        ;;
    *.tgz | *.tbz2 | *.txz)
        dirName="$(echo "$1" | rev | cut -d'.' -f 2- | rev)"
        mkdir "$dirName" &&
            tar -xzf "$1" -C "$dirName" &&
            echo Extracted files to "$dirName"
        ;;
    *.tar)
        dirName="$(echo "$1" | rev | cut -d'.' -f 2- | rev)"
        mkdir "$dirName" &&
            tar -xf "$1" -C "$dirName" &&
            echo Extracted files to "$dirName"
        ;;
    *.zip | *.7z | *.rar)
        dirName="$(echo "$1" | rev | cut -d'.' -f 2- | rev)"
        mkdir "$dirName" &&
            7z x "$1" -o"$dirName" &&
            echo Extracted files to "$dirName"
        ;;
esac